/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.mitaller.entidades;

import java.math.BigDecimal;
import java.util.List;

/**
 * Clase para representar un taller que tendrá un nombre y un registro de sus mecánicos y reparaciones realizadas
 * @author Rafael González Centeno
 */
public class Taller {
    private final List<Mecanico> mecanicos;
    private final List<Reparacion> reparaciones;
    private final String nombreTaller;

    /**
     * Crea un taller con el nombre pasado como parámetro
     * @param nombreTaller Nombre del taller
     */
    public Taller(String nombreTaller) {
        this.mecanicos = new java.util.ArrayList<>();
        this.reparaciones = new java.util.ArrayList<>();
        this.nombreTaller = nombreTaller;
    }
    
    /**
     * Añade un mecánico al taller
     * @param m el mecánico a añadir
     * @throws IllegalArgumentException si el mecánico es null
     */
    public void addMecanico(Mecanico m){
        if(m == null){
            throw new IllegalArgumentException("El mecánico a agregar no puede ser null");
        }
        mecanicos.add(m);        
    }
    
    /**
     * Agrega una reparación al taller
     * @param r Reparación a agregar. No permitido null.
     * @throws IllegalArgumentException si r es null
     */
    public void addReparacion(Reparacion r){
        reparaciones.add(r);
    }
    
    /**
     * Devuelve el coste de salario mensual para todos los trabajadores que existen en el taller
     * @return El salario total de los trabajadores
     */
    public java.math.BigDecimal getSalarioTrabajadores(){
        return mecanicos.stream().map(m -> m.getSueldo()).reduce(BigDecimal.ZERO, BigDecimal::add);
    }
    
    /**
     * Devuelve el importe total de todas las reparaciones que existen en el taller
     * @return Importe total de las reparaciones
     */
    public java.math.BigDecimal getImporteReparaciones(){
        return reparaciones.stream().filter(r -> r.getFechaSalida() != null).map(r -> r.getCosteReparacion()).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * Devuelve las reparaciones pendientes de realizar en el taller
     * @return Lista con todas las reparaciones pendientes
     */
    public java.util.List<Reparacion> getReparacionesPendientes(){
        return reparaciones.stream().filter(r -> r.getFechaSalida() == null).collect(java.util.stream.Collectors.toList());
    }
    
    /**
     * Devuelve el nombre del taller
     * @return nombre del taller
     */
    public String getNombreTaller() {
        return nombreTaller;
    }   
    
    /**
     * Incrementa el salario de todos los mecánicos en el porcentaje dado
     * @param porcentaje Porcentaje a aplicar en base 100. 3 sería el 3%.
     */
    public void incrementarSalario(double porcentaje){
        for(Mecanico m: mecanicos){
            m.incrementarSueldoIPC(porcentaje);
        }
    }
    
}
