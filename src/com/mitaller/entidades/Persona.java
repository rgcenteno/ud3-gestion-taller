/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.mitaller.entidades;

/**
 * Representa a una persona con nombre y DNI
 * @author Rafael González Centeno
 */
public class Persona {
    final private String nombreApellidos;
    final private String dni;

    /**
     * Crea una persona con el DNI y el nombre y apellidos dado
     * @param nombreApellidos Nombre de la persona
     * @param dni DNI de la persona
     */
    public Persona(String nombreApellidos, String dni) {
        this.nombreApellidos = nombreApellidos;
        checkDNI(dni);
        this.dni = dni;
    }

    /**
     * Devuelve el DNI de la persona
     * @return el dni de la persona
     */
    public String getDni() {
        return dni;
    }

    /**
     * El nombre de la persona
     * @return nombre de la persona
     */
    public String getNombreApellidos() {
        return nombreApellidos;
    }
    
    /**
     * Comprueba que un DNI es correcto
     * @param itDNI 
     */
    private static void checkDNI(String itDNI) {
        if(!com.mitaller.utils.CalculaNif.isvalido(itDNI)){
            throw new IllegalArgumentException("El DNI no es válido");
        }

    }
        
}
