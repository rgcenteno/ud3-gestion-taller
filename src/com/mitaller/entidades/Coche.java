/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.mitaller.entidades;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Clase que representa un coche con sus datos de identificación tales como matrícula y marca, modelo y parte de sus funcionalidades
 * @author Rafael González Centneo
 */
public class Coche {
    final private String matricula;
    final private String marca;
    final private String modelo;
    final private int cv;
    private LocalDate fechaMatriculacion;
    private double kms = 0;

    /**
     * Crea un coche con los parámetros especificados
     * @param matricula La matrícula del coche. Formato <b>LLLL000</b> donde L son letras y 0 son número
     * @param marca Marca del coche
     * @param modelo Modelo del coche
     * @param cv Los caballos. Valor mayor que cero.
     * @param fechaMatriculacion  Fecha de matriculación.
     * @throws IllegalArgumentException si se inserta una potencia de cero o menos cv o si la matrícula no es válida
     */
    public Coche(String matricula, String marca, String modelo, int cv, LocalDate fechaMatriculacion) {
        Coche.checkMatricula(matricula);
        this.matricula = matricula;
        this.marca = marca;
        this.modelo = modelo;
        Coche.checkCv(cv);
        this.cv = cv;     
        this.fechaMatriculacion = fechaMatriculacion;
    }
       
    /**
     * Comprueba que pasamos un parámetro válido para los CV
     * @param cv Caballos de potencia del coche
     * @throws IllegalArgumentException si se inserta una potencia de cero o menos cv
     */
    private static void checkCv(int cv){
        if(cv <= 0){
            throw new IllegalArgumentException("La potencia debe ser mayor que cero");
        }
    }
    
    private static void checkMatricula(String matricula){
        Pattern pt = Pattern.compile("[0-9]{4}[A-Z]{3}");
        Matcher mt = pt.matcher(matricula);
        if(!mt.matches()){
            throw new IllegalArgumentException("La matrícula debe tener formato LLLL000 donde L son letras y 0 son números");
        }
    }

    /**
     * Devuelve la matrícula del coche
     * @return the matricula
     */
    public String getMatricula() {
        return matricula;
    }

    /**
     * Devuelve la marca del coche
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Devuelve el modelo del coche
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * Devuelve los caballos de potencia del coche
     * @return Los caballos de potencia
     */
    public int getCv() {
        return cv;
    }

    /**
     * Devuelve la fecha de matriculación del coche
     * @return the fechaMatriculacion
     */
    public LocalDate getFechaMatriculacion() {
        return fechaMatriculacion;
    }

    /**
     * Establece la fecha de matriculación
     * @param fechaMatriculacion fecha que queremos establecer
     */
    public void setFechaMatriculacion(LocalDate fechaMatriculacion) {
        this.fechaMatriculacion = fechaMatriculacion;
    }
    
    /**
     * Realiza un viaje agregando los kms del mismo al contador kms
     * @param kmsViaje Los kilómetros que ha llevado el viaje. Debe ser mayor que cero.
     * @throws IllegalArgumentException si se inserta un viaje de cero o menos kms
     */
    public void viajar(double kmsViaje)
    {
        if(kmsViaje <= 0){
            throw new IllegalArgumentException("El viaje debe tener una distancia mayor que cero");
        }
        this.kms += kmsViaje;
    }

    /**
     * Devuelve los kms que ha viajado el coche
     * @return Kms que ha viajado el coche
     */
    public double getKms() {
        return kms;
    }

    @Override
    public String toString() {
        final DateTimeFormatter FORMATO_FECHA = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return "Marca: " + marca + "\nModelo: " + modelo + "\nMatrícula: " + matricula + "\nFecha Matriculación: " + fechaMatriculacion.format(FORMATO_FECHA) + "\nKms: " + this.kms;
    }        
    
}
