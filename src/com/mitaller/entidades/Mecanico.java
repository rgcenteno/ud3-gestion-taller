/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.mitaller.entidades;

import java.math.RoundingMode;

/**
 * Clase que representa a un mecánico. El mecánico es una clase con la que se pueden realizar las reparaciones de los coches.
 * @author Rafael González Centeno
 */
public class Mecanico extends Persona{
    private java.math.BigDecimal sueldo;
    private final String numEmpleado;

    /**
     * Crea un mecánico
     * @param numEmpleado Número de empleado del mecánico
     * @param dni DNI del mecánico
     * @param nombreApellidos Nombre completo
     * @param sueldo Debe de ser mayor que cero
     * @throws IllegalArgumentException Si el sueldo es cero o menor
     */
    public Mecanico(String numEmpleado, String dni, String nombreApellidos, double sueldo) {
        super(nombreApellidos, dni);
        checkSueldo(sueldo);
        this.sueldo = java.math.BigDecimal.valueOf(sueldo).setScale(2, RoundingMode.HALF_UP);
        this.numEmpleado = numEmpleado;
    }
    
    /**
     * Controla que el sueldo sea mayor o igual que cero
     * @param sueldo Sueldo a testear
     */
    private static void checkSueldo(double sueldo){
        if(sueldo <= 0){
            throw new IllegalArgumentException("El sueldo debe ser mayor que cero");
        }
    }

    /**
     * Devuelve el número de empleado
     * @return Número de empleado
     */
    public String getNumEmpleado() {
        return numEmpleado;
    }
    
    /**
     * Devuelve el sueldo del mecánico
     * @return Sueldo del trabajador
     */
    public java.math.BigDecimal getSueldo() {
        return sueldo;
    }
    
    /**
     * Incrementa el sueldo del trabajador
     * @param ipc IPC a aplicar. 3 sería 3%
     */
    public void incrementarSueldoIPC(double ipc){
        sueldo = sueldo.multiply(java.math.BigDecimal.valueOf(1 + ipc/100)).setScale(2, RoundingMode.HALF_UP);
    }
}
