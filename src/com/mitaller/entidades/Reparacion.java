/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.mitaller.entidades;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Entidad que representa una reparación pendiente o realizada con los datos de la misma: Coche, mecánico asignado, fecha de Salida, coste...
 * @author Rafael González Centeno
 */
public class Reparacion {
    final private Coche coche;
    final private String averia;
    private String reparacion;
    final private LocalDate fechaEntrada;
    private LocalDate fechaSalida;
    private Mecanico mecanicoReparacion;
    private java.math.BigDecimal costeReparacion;

    /**
     * Crea una reparación con el coche a reparar, la descripción de la avería y la fecha de entrada.
     * @param coche Coche a reparar. No puede ser null
     * @param averia Descripción de la avería
     * @param fechaEntrada Fecha de entrada
     * @throws IllegalArgumentException si el coche a reparar es null
     */
    public Reparacion(Coche coche, String averia, LocalDate fechaEntrada) {
        if(coche == null){
            throw new IllegalArgumentException("Es necesario asociar el coche a reparar");
        }
        this.coche = coche;            
        this.averia = averia;
        this.fechaEntrada = fechaEntrada;        
    }

    /**
     * Devuelve el coche que está siendo reparado
     * @return the coche Coche a reparar
     */
    public Coche getCoche() {
        return coche;
    }

    /**
     * Devuelve la avería que sufre el coche
     * @return the averia la avería que sufre el coche
     */
    public String getAveria() {
        return averia;
    }

    /**
     * Devuelve la reparación que sufre el coche
     * @return the reparacion que sufre el coche
     */
    public String getReparacion() {
        return reparacion;
    }

    /**
     * Devuelve la fecha de entrada del coche para la reparación
     * @return the fechaEntrada fecha de entrada del coche para la reparación
     */
    public java.time.LocalDate getFechaEntrada() {
        return fechaEntrada;
    }

    /**
     * fecha de salida del coche para la reparación
     * @return the fechaSalida fecha de salida
     */
    public java.time.LocalDate getFechaSalida() {
        return fechaSalida;
    }

    /**
     * Se ejecuta la reparación registrando los valores de coste, fecha salida, mecánico que realiza la reparación y el coste de la misma
     * @param reparacion Texto que describe la reparación
     * @param fecha Fecha en la que se finalizó la reparación
     * @param mecanico Mecánico encargado de la reparación. No puede ser null.
     * @param costeReparacion Coste de la reparación. No puede ser menor que cero
     * @throws IllegalArgumentException si no se asocia un mecánico o si el precio de la reparación es menor que cero.
     */
    public void reparar(String reparacion, java.time.LocalDate fecha, Mecanico mecanico, double costeReparacion) {
        if(mecanico == null){
            throw new IllegalArgumentException("Es necesario asociar un mecánico");
        }
        if(costeReparacion < 0){
            throw new IllegalArgumentException("El coste no puede ser menor que cero");
        }
        this.reparacion = reparacion;
        this.fechaSalida = fecha;
        this.mecanicoReparacion = mecanico;
        this.costeReparacion = java.math.BigDecimal.valueOf(costeReparacion).setScale(2, RoundingMode.HALF_UP);
    }

    /**
     * Devuelve el coste de la reparación
     * @return Coste de la reparación
     */
    public BigDecimal getCosteReparacion() {
        return costeReparacion;
    }        

    @Override
    public String toString() {
        final DateTimeFormatter FORMATO_FECHA = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        StringBuilder sb = new StringBuilder("Matrícula: " + coche.getMatricula() +"\n" + "Marca y modelo: " + coche.getMarca() + " " + coche.getModelo() + "\nFecha Entrada: " + this.fechaEntrada.format(FORMATO_FECHA)+"\nAvería: " + this.averia);
        if(this.mecanicoReparacion != null){
            sb.append("\nMecánico: ").append(this.mecanicoReparacion.getNumEmpleado()).append("\nReparación: ").append(this.reparacion).append("\nFecha Salida: ").append(this.fechaSalida.format(FORMATO_FECHA)).append("\nCoste: ").append(costeReparacion).append("€");
        }
        else{
            sb.append("\nEstado: Pendiente de reparación");
        }
        return sb.toString();
    }
    
   
    
    
}
